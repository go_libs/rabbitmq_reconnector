import (
        r "rabbitmq_reconnector"
        "github.com/streadway/amqp"
)

args := make(map[string]interface{})
args["x-dead-letter-exchange"] = "someex"
args["x-message-ttl"] = 30000

durable := true
autoack := false
queue = rq.NewQueue(configuration.Rabbitmq, configuration.Queue, durable, autoack, &args)
queue.Send("ewewew")

queue.Consume(func(i string, delivery ampq.Delivery) {
                log.Printf("Received message with second consumer: %s", i)
		delivery.Ack(true)
        })

