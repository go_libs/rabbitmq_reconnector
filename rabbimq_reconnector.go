package rabbitmq_reconnector

import (
	"github.com/streadway/amqp"
	"log"
	"time"
)

type Queue struct {
	url  string
	name string

	errorChannel chan *amqp.Error
	connection   *amqp.Connection
	channel      *amqp.Channel
	closed       bool
	durable	bool
	autoack bool
	args amqp.Table

	consumers []messageConsumer
}

type messageConsumer func(string, amqp.Delivery)

func NewQueue(url string, qName string, durable bool, autoack bool, args *map[string]interface{} ) *Queue {
	q := new(Queue)
	q.url = url
	q.name = qName
	q.consumers = make([]messageConsumer, 0)
	q.durable = durable
	q.autoack = autoack
	if args != nil {
		q.args = *args

	}

	q.connect()
	go q.reconnector()

	return q
}

func (q *Queue) Send(message string) {
	err := q.channel.Publish(
		"",     // exchange
		q.name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			DeliveryMode: 2,
			Body:        []byte(message),
		})
	logError("Sending message to Queue failed", err)
}

func (q *Queue) Consume(consumer messageConsumer) {
	log.Println("Registering consumer...")
	deliveries, err := q.registerQueueConsumer()
	log.Println("Consumer registered! Processing messages...")
	q.executeMessageConsumer(err, consumer, deliveries, false)
}

func (q *Queue) Close() {
	log.Println("Closing connection")
	q.closed = true
	q.channel.Close()
	q.connection.Close()
}

func (q *Queue) reconnector() {
	for {
		err := <-q.errorChannel
		if !q.closed {
			logError("Reconnecting after connection closed", err)

			q.connect()
			q.recoverConsumers()
		}
	}
}

func (q *Queue) connect() {
	for {
		log.Printf("Connecting to rabbitmq on %s\n", q.url)
		conn, err := amqp.Dial(q.url)
		if err == nil {
			q.connection = conn
			q.errorChannel = make(chan *amqp.Error)
			q.connection.NotifyClose(q.errorChannel)

			log.Println("Connection established!")

			q.openChannel()
			q.declareQueue()

			return
		}

		logError("Connection to rabbitmq failed. Retrying in 1 sec... ", err)
		time.Sleep(1000 * time.Millisecond)
	}
}

func (q *Queue) declareQueue() {
	args := q.args

	_, err := q.channel.QueueDeclare(
		q.name, // name
		q.durable,  // durable
		false,  // delete when unused
		false,  // exclusive
		false,  // no-wait
		args,    // arguments
	)
	logError("Queue declaration failed", err)
}

func (q *Queue) openChannel() {
	channel, err := q.connection.Channel()
	logError("Opening channel failed", err)
	q.channel = channel
}

func (q *Queue) registerQueueConsumer() (<-chan amqp.Delivery, error) {
	msgs, err := q.channel.Consume(
		q.name, // Queue
		"",     // messageConsumer
		q.autoack,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	logError("Consuming messages from Queue failed", err)
	return msgs, err
}

func (q *Queue) executeMessageConsumer(err error, consumer messageConsumer, deliveries <-chan amqp.Delivery, isRecovery bool) {
	if err == nil {
		if !isRecovery {
			q.consumers = append(q.consumers, consumer)
		}
		go func() {
			for delivery := range deliveries {
				consumer(string(delivery.Body[:]), delivery)
			}
		}()
	}
}

func (q *Queue) recoverConsumers() {
	for i := range q.consumers {
		var consumer = q.consumers[i]

		log.Println("Recovering consumer...")
		msgs, err := q.registerQueueConsumer()
		log.Println("Consumer recovered! Continuing message processing...")
		q.executeMessageConsumer(err, consumer, msgs, true)
	}
}

func logError(message string, err error) {
	if err != nil {
		log.Printf("%s: %s", message, err)
	}
}
